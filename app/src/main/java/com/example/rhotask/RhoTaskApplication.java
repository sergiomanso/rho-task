package com.example.rhotask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class RhoTaskApplication {

	 @RequestMapping("/")
  	public String home() {
    		return "RHO Task";
  	}

	public static void main(String[] args) {
		SpringApplication.run(RhoTaskApplication.class, args);
	}

}
