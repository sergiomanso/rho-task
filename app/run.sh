#!/bin/bash

IMAGE=rho-springbootws:latest
NAME=rhotask

./mvnw clean install

./mvnw package

docker build -t $IMAGE .

docker stop $NAME && docker rm $NAME

docker run --name $NAME -p 8080:8080 -d --privileged $IMAGE
